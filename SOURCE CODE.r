install.packages("lattice")
install.packages("ggplot2")
install.packages("Hmisc")

#loading values

vSeason =  DATAFERTILITY$'Season'
vAge =  DATAFERTILITY$'Age'
vChildish.diseases =  DATAFERTILITY$'Childish diseases'
vAccident.or.serious.trauma =  DATAFERTILITY$'Accident or serious trauma'
vSurgical.intervention =  DATAFERTILITY$'Surgical intervention'
vHigh.fevers.in.the.last.year =  DATAFERTILITY$'High fevers in the last year'
vFrequency.of.alcohol.consumption =  DATAFERTILITY$'Frequency of alcohol consumption'
vSmoking.habit =  DATAFERTILITY$'Smoking habit'
vNumber.of.hours.spent.sitting.per.day =  DATAFERTILITY$'Number of hours spent sitting per day'

vDiagnosis =  DATAFERTILITY$'Diagnosis'
vDiagnosis = as.factor( DATAFERTILITY$'Diagnosis')


#membuat data frame untuk model
df <- data.frame(vSeason,vAge,vChildish.diseases,vAccident.or.serious.trauma,vSurgical.intervention,
                 vHigh.fevers.in.the.last.year,vFrequency.of.alcohol.consumption,vSmoking.habit,
                 vNumber.of.hours.spent.sitting.per.day,vDiagnosis)



#explore data tiap field agar tahu apakah datanya sesuai atau tidak
boxplot(vSeason) #memvisualisasikan datanya dalam bentuk box
summary(vSeason) #menghitung nilai yang ada di field dan dicari tau nilai minimalnya,
                 #quarter pertama, median/nilai tengah/mean/kuarter ke3,max

boxplot(vAge) #memvisualisasikan datanya dalam bentuk box
summary(vAge)

boxplot(vChildish.diseases) #memvisualisasikan datanya dalam bentuk box
summary(vChildish.diseases)

boxplot(vAccident.or.serious.trauma) #memvisualisasikan datanya dalam bentuk box
summary(vAccident.or.serious.trauma)

boxplot(vSurgical.intervention) #memvisualisasikan datanya dalam bentuk box
summary(vSurgical.intervention)

boxplot(vHigh.fevers.in.the.last.year) #memvisualisasikan datanya dalam bentuk box
summary(vHigh.fevers.in.the.last.year)

boxplot(vFrequency.of.alcohol.consumption) #memvisualisasikan datanya dalam bentuk box
summary(vFrequency.of.alcohol.consumption)

boxplot(vSmoking.habit) #memvisualisasikan datanya dalam bentuk box
summary(vSmoking.habit)

boxplot(vNumber.of.hours.spent.sitting.per.day) #memvisualisasikan datanya dalam bentuk box
summary(vNumber.of.hours.spent.sitting.per.day)



#membuat penyusunan training dalam bentuk tabel dari fungsi df mulai baris 2 sampai 101 
train <- df[2:101,]

#
#membuat penyusunan data tes dalam bentuk tabel dari fungsi df mulai baris 2 sampai 101 
test <- df[102:131,]

library(lattice)
library(ggplot2)


#loading library for model
library(e1071) #for naive bayes
library(caret) #for confusion matrix

#model construction using naive bayes
model <- naiveBayes(vDiagnosis~.,data=train)

#prediksi using data training
prediksi <- predict(model,train)
hasilprediksi <- table(prediksi)
show(hasilprediksi)


#confusion matrix for train data
confusionMatrix(prediksi,train$vDiagnosis,dnn = list("predicted","actual"))





# Membuat grafik untuk menampilkan nilai variabel hasilpredteshasil berupa pie chart
xhasil <-  c(hasilprediksi)
labels <-  c("Altered","Normal")

piepercent<- round(100*xhasil/sum(xhasil), 1)

# Plot the chart.
pie(xhasil, labels = piepercent, main = "Diagram Pie Kesuburan Sperma",
    col = rainbow(length(xhasil)))
legend("topleft", c("Altered","Normal"), cex = 0.8,
       fill = rainbow(length(xhasil)))


#prediksi using data testing
predtes <- predict(model,test)
hasilpredtes <- table(predtes)
show(hasilpredtes)
View(predtes)







#=========================================================================================





library(Hmisc)
#loading values prob


vSeasonhasiltes =  DATAFERTILITYhasiltes$'Season'
vAgehasiltes =  DATAFERTILITYhasiltes$'Age'
vChildish.diseaseshasiltes =  DATAFERTILITYhasiltes$'Childish diseases'
vAccident.or.serious.traumahasiltes =  DATAFERTILITYhasiltes$'Accident or serious trauma'
vSurgical.interventionhasiltes =  DATAFERTILITYhasiltes$'Surgical intervention'
vHigh.fevers.in.the.last.yearhasiltes =  DATAFERTILITYhasiltes$'High fevers in the last year'
vFrequency.of.alcohol.consumptionhasiltes =  DATAFERTILITYhasiltes$'Frequency of alcohol consumption'
vSmoking.habithasiltes =  DATAFERTILITYhasiltes$'Smoking habit'
vNumber.of.hours.spent.sitting.per.dayhasiltes =  DATAFERTILITYhasiltes$'Number of hours spent sitting per day'

vDiagnosishasiltes =  DATAFERTILITYhasiltes$'Diagnosis'
vDiagnosishasiltes = as.factor( DATAFERTILITYhasiltes$'Diagnosis')


dfhasil <- data.frame(vSeason,vAge,vChildish.diseases,vAccident.or.serious.trauma,vSurgical.intervention,
                      vHigh.fevers.in.the.last.year,vFrequency.of.alcohol.consumption,
                      vSmoking.habit,vNumber.of.hours.spent.sitting.per.day,vDiagnosis)

#testing dataset
testhasil <- dfhasil[2:31,]

#loading library for model
library(e1071) #for naive bayes
library(caret) #for confusion matrix

#model construction using naive bayes
modelhasil <- naiveBayes(vDiagnosis~.,data=testhasil)

#prediksi using data testing
predhasiltes <- predict(modelhasil,testhasil)
hasilpredteshasil <- table(predhasiltes) //mentabelkan isi variabel predhasiltes
show(hasilpredteshasil)



#confusion matrix for tes data
confusionMatrix(predhasiltes,testhasil$vDiagnosis,dnn = list("predicted","actual"))



# Membuat grafik untuk menampilkan nilai variabel hasilpredteshasil berupa pie chart
xhasiltes <-  c(hasilpredteshasil)
labels <-  c("Altered","Normal")

piepercent<- round(100*xhasiltes/sum(xhasiltes), 1)

# Plot the chart.
pie(xhasiltes, labels = piepercent, main = "Diagram Pie  Hasil Tes",col = rainbow(length(xhasiltes)))
legend("topleft", c("Altered","Normal"), cex = 1,
       fill = rainbow(length(xhasiltes)))

